package com.chris.springdemo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wujf
 * @date 2024-07-18
 * @description 描述
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("alipay_info")
public class AlipayInfoEntity extends BaseEntity {

    private String appId;

    private String privateKey;

    private String alipayPublicKey;

}
