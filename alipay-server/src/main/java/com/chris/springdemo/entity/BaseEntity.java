package com.chris.springdemo.entity;

import lombok.Data;

import java.util.Date;

/**
 * @author wujf
 * @date 2024-07-18
 * @description 描述
 */
@Data
public abstract class BaseEntity {

    protected Long id;

    protected Date gmtCreate;

    protected Date gmtModified;

    protected Integer recordStatus;
}
