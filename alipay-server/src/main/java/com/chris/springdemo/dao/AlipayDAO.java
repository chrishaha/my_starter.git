package com.chris.springdemo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chris.springdemo.entity.AlipayInfoEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author wujf
 * @date 2024-07-18
 * @description 描述
 */
@Mapper
public interface AlipayDAO extends BaseMapper<AlipayInfoEntity> {
}
