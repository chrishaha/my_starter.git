package com.chris.springdemo.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.chris.springdemo.dao.AlipayDAO;
import com.chris.springdemo.entity.AlipayInfoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author wujf
 * @date 2021/11/16
 */
@Service
public class TestService {

    @Autowired
    private AlipayDAO alipayDAO;

    public List<AlipayInfoEntity> listInfo() {
        Wrapper<AlipayInfoEntity> queryWrapper = new LambdaQueryWrapper<>();
        return alipayDAO.selectList(queryWrapper);
    }
}
