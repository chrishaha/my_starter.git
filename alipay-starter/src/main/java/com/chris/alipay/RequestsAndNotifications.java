package com.chris.alipay;

import org.springframework.util.StringUtils;

import java.util.function.Function;

/**
 * @author wujf
 * @date 2024-06-07
 * @description 描述
 */
public abstract class RequestsAndNotifications<T, R> {

    private String currentClassName;

    public RequestsAndNotifications() {
        this.currentClassName = StringUtils.uncapitalize(this.getClass().getName().substring(this.getClass().getName().lastIndexOf('.') + 1));
    }

    public final String execute(int a) {
        String request = request(a);
        return request + "_" + currentClassName;
    }

    public abstract String request(int a);

    public abstract Function<T, R> notification();
}
