package com.chris.alipay;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.Objects;
import java.util.function.Consumer;


public class JedisCore {

    private JedisPool jedisPool;

    public JedisCore(JedisPool jedisPool) {
        this.jedisPool = jedisPool;
    }

    public void executeJedis(Consumer<Jedis> consumer) {
        if (Objects.isNull(jedisPool)) {
            throw new RuntimeException("jedisPool为空");
        }
        try (Jedis jedis = jedisPool.getResource()) {
            consumer.accept(jedis);
        } catch (Exception e) {
            throw new RuntimeException("jedis错误");
        }
    }
}
