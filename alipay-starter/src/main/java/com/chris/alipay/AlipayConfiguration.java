package com.chris.alipay;

import com.chris.alipay.controller.AlipayController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Configuration
@Import({AlipayController.class})
// 取jedis.enable的配置和 havingValue 后的值对比 相同的话则生效启用 matchIfMissing=false表示默认不配置jedis.enable不生效
@ConditionalOnProperty(prefix = "alipay", name = "enable", havingValue = "true", matchIfMissing = true)
public class AlipayConfiguration {

    public static final Map<String, Function<String, Integer>> notificationMap = new HashMap<>();

    @Autowired
    private Map<String, RequestsAndNotifications> map;

    @PostConstruct
    public void init() {
        map.forEach((k, v) -> {
            notificationMap.put(k, v.notification());
        });
    }

}
