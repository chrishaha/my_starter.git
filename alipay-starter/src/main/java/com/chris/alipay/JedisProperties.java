package com.chris.alipay;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "jedis.config")
@Data
public class JedisProperties {
    private String host = "127.0.0.1";
    private int port = 6379;
    private String password;
    private int maxTotal = 8;
    private int maxIdle = 8;
    private int minIdle = 0;
    private int timeout = 1000;
}
