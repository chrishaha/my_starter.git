package com.chris.alipay.controller;

import com.chris.alipay.AlipayConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wujf
 * @date 2024-06-07
 * @description 描述
 */
@RestController
public class AlipayController {

    @GetMapping("/chris/{className}")
    public String chris(@PathVariable(name = "className") String className) {
        String data = "123";
        AlipayConfiguration.notificationMap.get(className).apply(data);
        return "chris";
    }
}
